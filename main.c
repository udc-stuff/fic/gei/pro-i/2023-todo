#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

struct fecha {
	int dia, mes, ano;
};

struct tarea {
	char titulo[20];
	bool finalizado;
	struct fecha fecha;
};

void fecha_actual(struct fecha *f) {
	time_t timestamp = time(NULL);
	struct tm *actual = localtime(&timestamp);
	
	f->dia = actual->tm_mday;
	f->mes = actual->tm_mon + 1;
	f->ano = actual->tm_year + 1900;
}

bool es_formato_fecha(char *s) {
	int cnt;
	int slash = 0;
	bool last_slash = true;

	for (cnt = 0; s[cnt] != '\0'; ++cnt) {
		if (s[cnt] == '/') {
			if (last_slash)
				return false;
			slash++;
			last_slash = true;
		} else if (s[cnt] < '0' || s[cnt] > '9') {
			return false;
		} else
			last_slash = false;
		
		if (slash > 2)
			return false;
	}

	return true;
}

bool es_fecha_valida(struct fecha f) {
	// comprobar numeros cero o negativos
	if (f.dia < 1 || f.mes < 1 || f.ano < 0)
		return false;

	if (f.mes > 12)
		return false;

	if (f.dia > 31)
		return false;

	// comparar los meses que tienen 30 dias y febrero (con el caso del ano bisiesto)
        // esta en un ejercicio del boletin 2

	return true;
}

void str_to_fecha(char *str, struct fecha *f) {
	char *aux;
	char *ptr = strchr(str, '/');

	*ptr = '\0';
	f->dia = atoi(str);

	ptr++;
	aux = strchr(ptr, '/');
	*aux = '\0';
	f->mes = atoi(ptr);

	aux++;
	f->ano = atoi(aux);
}

void pedir_tarea(struct tarea *t) {
	char str[15];
	char *ptr;

	printf("Titulo de la tarea: ");
	scanf("%s", t->titulo);
	fflush(stdin);
	printf("Introduce la fecha para realizar realizar la tarea (dd/mm/aaaa) (en blanco para la fecha actual):");
	//scanf("%d/%d/%d", &t->fecha.dia, &t->fecha.mes, &t->fecha.ano);
	fgets(str, 15, stdin);
	ptr = strchr(str, '\n');
	if (ptr != NULL)
		*ptr = '\0';
	if (0 == strcmp("", str))
		fecha_actual(&t->fecha);
	else {
		if (!es_formato_fecha(str))
			printf("%s: el formato de fecha no es valido\n", str);
		else {
			str_to_fecha(str, &t->fecha);
			if (!es_fecha_valida(t->fecha)) {
				printf("%s: el formato es valido, pero los valores no\n", str);
			}
		}
	}

	t->finalizado = false;
}

void pedir_tareas(int size, struct tarea array[]) {
	int cnt;

	for (cnt = 0; cnt < size; ++cnt) {
		printf("Tarea %d\n", cnt+1);
		pedir_tarea(&array[cnt]);
	}
	
}

void mostrar_tarea(struct tarea ta) {
	printf("TAREA: '%s [%2d/%2d/%4d]' (finalizado: %s)\n", 
		ta.titulo, ta.fecha.dia, ta.fecha.mes, ta.fecha.ano, ta.finalizado? "Si": "No" );
}

void mostrar_tareas(int size, struct tarea array[]) {
	int cnt;
	for (cnt = 0; cnt < size; ++cnt)
		mostrar_tarea(array[cnt]);
}

int main() {
	const int N_TAREAS = 4;
	struct tarea tareas[N_TAREAS];

	pedir_tareas(N_TAREAS, tareas);
	mostrar_tareas(N_TAREAS, tareas);

	return 0;
}
